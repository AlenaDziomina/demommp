package org.example.mapper;

import org.example.dto.UserDto;
import org.example.entity.UserEntity;
import org.example.util.NameUtil;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "emailAddress", source = "dto.email")
    UserEntity mapToEntity(UserDto dto);

    @AfterMapping
    default void fullName(@MappingTarget UserEntity userEntity) {
        String fullName = NameUtil.getFullName(userEntity.getFirstName(), userEntity.getLastName());
        userEntity.setFullName(fullName);
    }

}
